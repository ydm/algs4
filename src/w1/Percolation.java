package w1;

import edu.princeton.cs.algs4.UF;

public class Percolation {
	public Percolation(int n) {
		side = n;
		source = n * n;
		grid = new boolean[source];
		numOpen = 0;
		union = new UF(n * n + 1);
		percs = false;
	}

	public void open(int row, int col) {
		int index = index(row, col);
		if (grid[index]) {
			return;
		}
		grid[index] = true;
		numOpen += 1;

		if (row == 1) {
			// first row
			union.union(index, source);
		}

		int neighbours[] = { //
				index - 1, // left
				index + 1, // right
				index - side, // top
				index + side, // bot
		};
		for (int i = 0; i < 4; i++) {
			int n = neighbours[i];
			if (check(n) && grid[n]) {
				union.union(index, n);
			}
		}
		
		for (int i = source - side - 1; i < source; i++) {
			if (grid[i] && union.connected(i, source)) {
				percs = true;
				return;
			}
		}
	}

	public boolean isOpen(int row, int col) {
		return grid[index(row, col)];
	}

	public boolean isFull(int row, int col) {
		return union.connected(index(row, col), source);
	}

	public int numberOfOpenSites() {
		return numOpen;
	}

	public boolean percolates() {
		return percs;
	}

	private int index(int row, int col) {
		return (row - 1) * side + (col - 1);
	}

	private boolean check(int index) {
		return 0 <= index && index < source;
	}

	private final int side;
	private final int source;
	private boolean grid[];
	private int numOpen;
	private UF union;
	private boolean percs;
}
